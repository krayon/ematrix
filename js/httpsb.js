/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 Raymond Hill
    Copyright (C) 2019-2020 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

/* global chrome, ηMatrix */

'use strict';

/******************************************************************************/

(function() {
    Cu.import('chrome://ematrix/content/lib/UriTools.jsm');

    var ηm = ηMatrix;
    ηm.pMatrix = new ηm.Matrix();
    ηm.pMatrix.setSwitch('matrix-off', 'about-scheme', 1);
    ηm.pMatrix.setSwitch('matrix-off', 'chrome-extension-scheme', 1);
    ηm.pMatrix.setSwitch('matrix-off', 'chrome-scheme', 1);
    ηm.pMatrix.setSwitch('matrix-off', 'moz-extension-scheme', 1);
    ηm.pMatrix.setSwitch('matrix-off', 'opera-scheme', 1);
    // https://discourse.mozilla.org/t/support-umatrix/5131/157
    ηm.pMatrix.setSwitch('matrix-off', 'wyciwyg-scheme', 1);
    ηm.pMatrix.setSwitch('matrix-off', 'behind-the-scene', 1);
    ηm.pMatrix.setSwitch('referrer-spoof', 'behind-the-scene', 2);
    ηm.pMatrix.setSwitch('https-strict', 'behind-the-scene', 2);
    // Global rules
    ηm.pMatrix.setSwitch('referrer-spoof', '*', 1);
    ηm.pMatrix.setSwitch('noscript-spoof', '*', 1);
    ηm.pMatrix.setCell('*', '*', '*', ηm.Matrix.Red);
    ηm.pMatrix.setCell('*', '*', 'css', ηm.Matrix.Green);
    ηm.pMatrix.setCell('*', '*', 'image', ηm.Matrix.Green);
    ηm.pMatrix.setCell('*', '*', 'frame', ηm.Matrix.Red);
    // 1st-party rules
    ηm.pMatrix.setCell('*', '1st-party', '*', ηm.Matrix.Green);
    ηm.pMatrix.setCell('*', '1st-party', 'frame', ηm.Matrix.Green);

    ηm.tMatrix = new ηm.Matrix();
    ηm.tMatrix.assign(ηm.pMatrix);
})();

/******************************************************************************/

ηMatrix.hostnameFromURL = function(url) {
    var hn = UriTools.hostnameFromURI(url);
    return hn === '' ? '*' : hn;
};

ηMatrix.scopeFromURL = ηMatrix.hostnameFromURL;

/******************************************************************************/

ηMatrix.evaluateURL = function(srcURL, desHostname, type) {
    var srcHostname = UriTools.hostnameFromURI(srcURL);
    return this.tMatrix.evaluateCellZXY(srcHostname, desHostname, type);
};


/******************************************************************************/

// Whitelist something

ηMatrix.whitelistTemporarily = function(srcHostname, desHostname, type) {
    this.tMatrix.whitelistCell(srcHostname, desHostname, type);
};

ηMatrix.whitelistPermanently = function(srcHostname, desHostname, type) {
    if ( this.pMatrix.whitelistCell(srcHostname, desHostname, type) ) {
        this.saveMatrix();
    }
};

/******************************************************************************/

// Auto-whitelisting the `all` cell is a serious action, hence this will be
// done only from within a scope.

ηMatrix.autoWhitelistAllTemporarily = function(pageURL) {
    var srcHostname = UriTools.hostnameFromURI(pageURL);
    if ( this.mustBlock(srcHostname, '*', '*') === false ) {
        return false;
    }
    this.tMatrix.whitelistCell(srcHostname, '*', '*');
    return true;
};

/******************************************************************************/

// Blacklist something

ηMatrix.blacklistTemporarily = function(srcHostname, desHostname, type) {
    this.tMatrix.blacklistCell(srcHostname, desHostname, type);
};

ηMatrix.blacklistPermanently = function(srcHostname, desHostname, type) {
    if ( this.pMatrix.blacklist(srcHostname, desHostname, type) ) {
        this.saveMatrix();
    }
};

/******************************************************************************/

// Remove something from both black and white lists.

ηMatrix.graylistTemporarily = function(srcHostname, desHostname, type) {
    this.tMatrix.graylistCell(srcHostname, desHostname, type);
};

ηMatrix.graylistPermanently = function(srcHostname, desHostname, type) {
    if ( this.pMatrix.graylistCell(srcHostname, desHostname, type) ) {
        this.saveMatrix();
    }
};

/******************************************************************************/

// TODO: Should type be transposed by the caller or in place here? Not an
// issue at this point but to keep in mind as this function is called
// more and more from different places.

ηMatrix.filterRequest = function(fromURL, type, toURL) {
    // Block request?
    var srcHostname = this.hostnameFromURL(fromURL);
    var desHostname = this.hostnameFromURL(toURL);

    // If no valid hostname, use the hostname of the source.
    // For example, this case can happen with data URI.
    if ( desHostname === '' ) {
        desHostname = srcHostname;
    }

    // Blocked by matrix filtering?
    return this.mustBlock(srcHostname, desHostname, type);
};

/******************************************************************************/

ηMatrix.mustBlock = function(srcHostname, desHostname, type) {
    return this.tMatrix.mustBlock(srcHostname, desHostname, type);
};

ηMatrix.mustAllow = function(srcHostname, desHostname, type) {
    return this.mustBlock(srcHostname, desHostname, type) === false;
};

/******************************************************************************/

// Commit temporary permissions.

ηMatrix.commitPermissions = function(persist) {
    this.pMatrix.assign(this.tMatrix);
    if ( persist ) {
        this.saveMatrix();
    }
};

/******************************************************************************/

// Reset all rules to their default state.

ηMatrix.revertAllRules = function() {
    this.tMatrix.assign(this.pMatrix);
};

/******************************************************************************/

ηMatrix.turnOff = function() {
    vAPI.app.start();
};

ηMatrix.turnOn = function() {
    vAPI.app.stop();
};

/******************************************************************************/

ηMatrix.formatCount = function(count) {
    if ( typeof count !== 'number' ) {
        return '';
    }
    var s = count.toFixed(0);
    if ( count >= 1000 ) {
        if ( count < 10000 ) {
            s = '>' + s.slice(0,1) + 'K';
        } else if ( count < 100000 ) {
            s = s.slice(0,2) + 'K';
        } else if ( count < 1000000 ) {
            s = s.slice(0,3) + 'K';
        } else if ( count < 10000000 ) {
            s = s.slice(0,1) + 'M';
        } else {
            s = s.slice(0,-6) + 'M';
        }
    }
    return s;
};

