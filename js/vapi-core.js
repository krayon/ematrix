/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 The uMatrix/uBlock Origin authors
    Copyright (C) 2019-2020 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

/******************************************************************************/

(function (self) {
    vAPI.modernFirefox =
	Services.appinfo.ID === '{ec8030f7-c20a-464f-9b0e-13a3a9e97384}'
	&& Services.vc.compare(Services.appinfo.version, '44') > 0;

    vAPI.app = {
	name: 'eMatrix',
	version: location.hash.slice(1),
	
	start: function () {
	    return;
	},
	stop: function () {
	    return;
	},
	restart: function () {
	    Cc['@mozilla.org/childprocessmessagemanager;1']
		.getService(Ci.nsIMessageSender)
		.sendAsyncMessage(location.host + '-restart');
	},
    };
    
    // List of things that needs to be destroyed when disabling the extension
    // Only functions should be added to it
    // eMatrix: taken care by vAPI.addCleanUpTask --- use that function
    let cleanupTasks = [];

    // This must be updated manually, every time a new task is added/removed
    // eMatrix: do we?
    let expectedNumberOfCleanups = 7;

    vAPI.addCleanUpTask = function (task) {
	if (typeof task !== 'function') {
	    return;
	}

	cleanupTasks.push(task);
    };

    vAPI.deferUntil = function (testFn, mainFn, details) {
	let dtls = (typeof details !== 'object') ? {} : details;
	let now = 0;
	let next = dtls.next || 200;
	let until = dtls.until || 2000;

	let check = function () {
            if (testFn() === true || now >= until) {
		mainFn();
		return;
            }
            now += next;
            vAPI.setTimeout(check, next);
	};

	if ('sync' in dtls && dtls.sync === true) {
            check();
	} else {
            vAPI.setTimeout(check, 1);
	}
    };

    window.addEventListener('unload', function () {
	// if (typeof vAPI.app.onShutdown === 'function') {
        //     vAPI.app.onShutdown();
	// }

	// IMPORTANT: cleanup tasks must be executed using LIFO order.
	for (let i=cleanupTasks.length-1; i>=0; --i) {
	    try {
		cleanupTasks[i]();
	    } catch (e) {
		// Just in case a clean up task ends up throwing for
		// no reason
		console.error(e);
	    }
	}

	// eMatrix: temporarily disabled
	// if (cleanupTasks.length < expectedNumberOfCleanups) {
        //     console.error
	//     ('eMatrix> Cleanup tasks performed: %s (out of %s)',
        //      cleanupTasks.length,
        //      expectedNumberOfCleanups);
	// }

	// frameModule needs to be cleared too
	Cu.import('chrome://ematrix/content/lib/FrameModule.jsm');
	contentObserver.unregister();
	Cu.unload('chrome://ematrix/content/lib/FrameModule.jsm');
    });

    vAPI.noTabId = '-1';

    vAPI.isBehindTheSceneTabId = function (tabId) {
	return tabId.toString() === '-1';
    };

    vAPI.lastError = function () {
	return null;
    };
})(this);
