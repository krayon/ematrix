/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2015-2019 Raymond Hill
    Copyright (C) 2019-2020 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/sessbench
*/

'use strict';

(function () {
    let tbody = document.querySelector('#content tbody');
    let trJunkyard = [];
    let tdJunkyard = [];
    let firstVarDataCol = 2;  // currently, column 2 (0-based index)
    let lastVarDataIndex = 3; // currently, d0-d3
    let maxEntries = 0;
    let noTabId = '';
    let allTabIds = {};
    let allTabIdsToken;
    let ownerId = Date.now();

    let emphasizeTemplate = document.querySelector('#emphasizeTemplate > span');
    let hiddenTemplate = document.querySelector('#hiddenTemplate > span');

    let prettyRequestTypes = {
	'main_frame': 'doc',
	'stylesheet': 'css',
	'sub_frame': 'frame',
	'xmlhttprequest': 'xhr'
    };

    let dontEmphasizeSet = new Set([
	'COOKIE',
	'CSP',
	'REFERER'
    ]);

    // Adjust top padding of content table, to match that of toolbar height.

    document
	.getElementById('content')
	.style
	.setProperty('margin-top',
		     document.getElementById('toolbar').clientHeight + 'px');

    let classNameFromTabId = function (tabId) {
	if (tabId === noTabId) {
            return 'tab_bts';
	}
	if (tabId !== '') {
            return 'tab_' + tabId;
	}
	return '';
    };

    // Emphasize hostname and cookie name.

    let emphasizeCookie = function (s) {
	let pnode = emphasizeHostname(s);
	if (pnode.childNodes.length !== 3) {
            return pnode;
	}
	
	let prefix = '-cookie:';
	let text = pnode.childNodes[2].textContent;
	
	let beg = text.indexOf(prefix);
	if (beg === -1) {
            return pnode;
	}
	beg += prefix.length;
	
	let end = text.indexOf('}', beg);
	if (end === -1) {
            return pnode;
	}
	
	let cnode = emphasizeTemplate.cloneNode(true);
	cnode.childNodes[0].textContent = text.slice(0, beg);
	cnode.childNodes[1].textContent = text.slice(beg, end);
	cnode.childNodes[2].textContent = text.slice(end);
	pnode.replaceChild(cnode.childNodes[0], pnode.childNodes[2]);
	pnode.appendChild(cnode.childNodes[0]);
	pnode.appendChild(cnode.childNodes[0]);
	
	return pnode;
    };

    // Emphasize hostname in URL.

    let emphasizeHostname = function (url) {
	let hnbeg = url.indexOf('://');
	if (hnbeg === -1) {
            return document.createTextNode(url);
	}
	hnbeg += 3;

	let hnend = url.indexOf('/', hnbeg);
	if (hnend === -1) {
            hnend = url.slice(hnbeg).search(/\?#/);
            if (hnend !== -1) {
		hnend += hnbeg;
            } else {
		hnend = url.length;
            }
	}

	let node = emphasizeTemplate.cloneNode(true);
	node.childNodes[0].textContent = url.slice(0, hnbeg);
	node.childNodes[1].textContent = url.slice(hnbeg, hnend);
	node.childNodes[2].textContent = url.slice(hnend);
	
	return node;
    };

    let createCellAt = function (tr, index) {
	let td = tr.cells[index];
	let mustAppend = !td;
	if (mustAppend) {
            td = tdJunkyard.pop();
	}
	
	if (td) {
            td.removeAttribute('colspan');
            td.textContent = '';
	} else {
            td = document.createElement('td');
	}
	if (mustAppend) {
            tr.appendChild(td);
	}
	
	return td;
    };

    let createRow = function (layout) {
	let tr = trJunkyard.pop();
	if (tr) {
            tr.className = '';
	} else {
            tr = document.createElement('tr');
	}

	let index;
	for (index=0; index<firstVarDataCol; ++index) {
            createCellAt(tr, index);
	}
	
	let i = 1, span = 1, td;
	for (;;) {
            td = createCellAt(tr, index);
            if (i === lastVarDataIndex) {
		break;
            }
            if (layout.charAt(i) !== '1') {
		span += 1;
            } else {
		if (span !== 1) {
                    td.setAttribute('colspan', span);
		}
		index += 1;
		span = 1;
            }
            i += 1;
	}
	
	if (span !== 1) {
            td.setAttribute('colspan', span);
	}
	index += 1;
	while ((td = tr.cells[index])) {
            tdJunkyard.push(tr.removeChild(td));
	}
	
	return tr;
    };

    let createHiddenTextNode = function (text) {
	let node = hiddenTemplate.cloneNode(true);
	node.textContent = text;
	return node;
    };

    let padTo2 = function (v) {
	return v < 10 ? '0' + v : v;
    };

    let createGap = function (tabId, url) {
	let tr = createRow('1');
	tr.classList.add('doc');
	tr.classList.add('tab');
	tr.classList.add('canMtx');
	tr.classList.add('tab_' + tabId);
	tr.cells[firstVarDataCol].textContent = url;
	tbody.insertBefore(tr, tbody.firstChild);
    };

    let renderLogEntry = function (entry) {
	let tr;
	let fvdc = firstVarDataCol;

	switch (entry.cat) {
	case 'error':
	case 'info':
            tr = createRow('1');
            if (entry.d0 === 'cookie') {
		tr.cells[fvdc].appendChild(emphasizeCookie(entry.d1));
            } else {
		tr.cells[fvdc].textContent = entry.d0;
            }
            break;
	case 'net':
            tr = createRow('111');
            tr.classList.add('canMtx');
	    
            // If the request is that of a root frame, insert a gap in the table
            // in order to visually separate entries for different documents. 
            if (entry.d2 === 'doc' && entry.tab !== noTabId) {
		createGap(entry.tab, entry.d1);
            }
	    
            if (entry.d3) {
		tr.classList.add('blocked');
		tr.cells[fvdc].textContent = '--';
            } else {
		tr.cells[fvdc].textContent = '';
            }
	    
            tr.cells[fvdc+1].textContent =
		(prettyRequestTypes[entry.d2] || entry.d2);
	    
            if (dontEmphasizeSet.has(entry.d2)) {
		tr.cells[fvdc+2].textContent = entry.d1;
            } else if ( entry.d2 === 'cookie' ) {
		tr.cells[fvdc+2].appendChild(emphasizeCookie(entry.d1));
            } else {
		tr.cells[fvdc+2].appendChild(emphasizeHostname(entry.d1));
            }
            break;
	default:
            tr = createRow('1');
            tr.cells[fvdc].textContent = entry.d0;
            break;
	}

	// Fields common to all rows.
	let time = logDate;
	time.setTime(entry.tstamp - logDateTimezoneOffset);
	tr.cells[0].textContent = padTo2(time.getUTCHours())
	    + ':'
	    + padTo2(time.getUTCMinutes())
	    + ':'
	    + padTo2(time.getSeconds());

	if (entry.tab) {
            tr.classList.add('tab');
            tr.classList.add(classNameFromTabId(entry.tab));
            if (entry.tab === noTabId) {
		tr.cells[1].appendChild(createHiddenTextNode('bts'));
            }
	}
	if (entry.cat !== '') {
            tr.classList.add('cat_' + entry.cat);
	}

	rowFilterer.filterOne(tr, true);

	tbody.insertBefore(tr, tbody.firstChild);
    };

    // Reuse date objects.
    let logDate = new Date();
    let logDateTimezoneOffset = logDate.getTimezoneOffset() * 60000;

    let renderLogEntries = function (response) {
	let entries = response.entries;
	if (entries.length === 0) {
            return;
	}

	// Preserve scroll position
	let height = tbody.offsetHeight;

	let tabIds = response.tabIds;
	let n = entries.length;
	let entry;
	for (let i=0; i<n; ++i) {
            entry = entries[i];
            // Unlikely, but it may happen
            if (entry.tab && tabIds.hasOwnProperty(entry.tab) === false) {
		continue;
            }
            renderLogEntry(entries[i]);
	}

	// Prevent logger from growing infinitely and eating all memory. For
	// instance someone could forget that it is left opened for some
	// dynamically refreshed pages.
	truncateLog(maxEntries);

	let yDelta = tbody.offsetHeight - height;
	if (yDelta === 0) {
            return;
	}

	// Chromium:
	//   body.scrollTop = good value
	//   body.parentNode.scrollTop = 0
	// if (document.body.scrollTop !== 0) {
        //     document.body.scrollTop += yDelta;
        //     return;
	// }

	// Firefox:
	//   body.scrollTop = 0
	//   body.parentNode.scrollTop = good value
	let parentNode = document.body.parentNode;
	if (parentNode && parentNode.scrollTop !== 0) {
            parentNode.scrollTop += yDelta;
	}
    };

    let synchronizeTabIds = function (newTabIds) {
	let oldTabIds = allTabIds;
	let autoDeleteVoidRows =
	    !!vAPI.localStorage.getItem('loggerAutoDeleteVoidRows');
	let rowVoided = false;
	let trs;
	
	for (let tabId in oldTabIds) {
            if (oldTabIds.hasOwnProperty(tabId) === false) {
		continue;
            }
            if (newTabIds.hasOwnProperty(tabId)) {
		continue;
            }
            // Mark or remove voided rows
            trs = uDom('.tab_' + tabId);
	    
            if (autoDeleteVoidRows) {
		toJunkyard(trs);
            } else {
		trs.removeClass('canMtx');
		rowVoided = true;
            }
	    
            // Remove popup if it is currently bound to a removed tab.
            if (tabId === popupManager.tabId) {
		popupManager.toggleOff();
            }
	}

	let select = document.getElementById('pageSelector');
	let selectValue = select.value;
	let tabIds = Object.keys(newTabIds).sort(function (a, b) {
            return newTabIds[a].localeCompare(newTabIds[b]);
	});

	let i, j;
	for (i=0, j=2; i<tabIds.length; ++i) {
            let tabId = tabIds[i];
            if (tabId === noTabId) {
		continue;
            }
	    
            let option = select.options[j];
            j += 1;
	    
            if (!option) {
		option = document.createElement('option');
		select.appendChild(option);
            }
	    
            option.textContent = newTabIds[tabId];
            option.value = classNameFromTabId(tabId);
	    
            if (option.value === selectValue) {
		option.setAttribute('selected', '');
            } else {
		option.removeAttribute('selected');
            }
	}
	
	while (j < select.options.length) {
            select.removeChild(select.options[j]);
	}
	
	if (select.value !== selectValue) {
            select.selectedIndex = 0;
            select.value = '';
            select.options[0].setAttribute('selected', '');
            pageSelectorChanged();
	}

	allTabIds = newTabIds;

	return rowVoided;
    };

    let truncateLog = function (size) {
	if (size === 0) {
            size = 5000;
	}
	
	let tbody = document.querySelector('#content tbody');
	size = Math.min(size, 10000);
	
	while (tbody.childElementCount > size) {
            let tr = tbody.lastElementChild;
            trJunkyard.push(tbody.removeChild(tr));
	}
    };

    let onLogBufferRead = function (response) {
	if (!response || response.unavailable) {
            readLogBufferAsync();
            return;
	}

	// This tells us the behind-the-scene tab id
	noTabId = response.noTabId;

	// This may have changed meanwhile
	if (response.maxLoggedRequests !== maxEntries) {
            maxEntries = response.maxLoggedRequests;
            uDom('#maxEntries').val(maxEntries || '');
	}

	// Neuter rows for which a tab does not exist anymore
	let rowVoided = false;
	if (response.tabIdsToken !== allTabIdsToken) {
            rowVoided = synchronizeTabIds(response.tabIds);
            allTabIdsToken = response.tabIdsToken;
	}

	renderLogEntries(response);

	if (rowVoided) {
            uDom('#clean')
		.toggleClass('disabled',
			     tbody
			     .querySelector('tr.tab:not(.canMtx)') === null);
	}

	// Synchronize toolbar with content of log
	uDom('#clear').toggleClass('disabled',
				   tbody.querySelector('tr') === null);

	readLogBufferAsync();
    };

    // This can be called only once, at init time. After that, this
    // will be called automatically. If called after init time, this
    // will be messy, and this would require a bit more code to ensure
    // no multi time out events.

    let readLogBuffer = function () {
	if (ownerId === undefined) {
	    return;
	}
	
	vAPI.messaging.send('logger-ui.js', {
	    what: 'readMany',
	    ownerId: ownerId
	}, onLogBufferRead);
    };

    let readLogBufferAsync = function () {
	if (ownerId === undefined) {
	    return;
	}
	vAPI.setTimeout(readLogBuffer, 1200);
    };

    let pageSelectorChanged = function () {
	let style = document.getElementById('tabFilterer');
	let tabClass = document.getElementById('pageSelector').value;
	let sheet = style.sheet;
	
	while (sheet.cssRules.length !== 0)  {
            sheet.deleteRule(0);
	}
	
	if (tabClass !== '') {
            sheet.insertRule('#content table tr:not(.'
			     + tabClass
			     + ') { display: none; }', 0);
	}
	uDom('#refresh').toggleClass('disabled',
				     tabClass === '' || tabClass === 'tab_bts');
    };

    let refreshTab = function () {
	let tabClass = document.getElementById('pageSelector').value;
	let matches = tabClass.match(/^tab_(.+)$/);
	if (matches === null) {
            return;
	}
	
	if (matches[1] === 'bts') {
            return;
	}
	
	vAPI.messaging.send('logger-ui.js', {
	    what: 'forceReloadTab',
	    tabId: matches[1]
	});
    };

    let onMaxEntriesChanged = function () {
	let raw = uDom(this).val();
	
	try {
            maxEntries = parseInt(raw, 10);
            if (isNaN(maxEntries)) {
		maxEntries = 0;
            }
	} catch (e) {
            maxEntries = 0;
	}

	vAPI.messaging.send('logger-ui.js', {
            what: 'userSettings',
            name: 'maxLoggedRequests',
            value: maxEntries
	});

	truncateLog(maxEntries);
    };

    let rowFilterer = (function () {
	let filters = [];

	let parseInput = function () {
            filters = [];

            let rawPart, hardBeg, hardEnd;
            let raw = uDom('#filterInput').val().trim();
            let rawParts = raw.split(/\s+/);
            let reStr, reStrs = [], not = false;
            let n = rawParts.length;
	    
            for (let i=0; i<n; ++i) {
		rawPart = rawParts[i];
		if (rawPart.charAt(0) === '!') {
                    if (reStrs.length === 0) {
			not = true;
                    }
                    rawPart = rawPart.slice(1);
		}
		
		hardBeg = rawPart.charAt(0) === '|';
		if (hardBeg) {
                    rawPart = rawPart.slice(1);
		}
		
		hardEnd = rawPart.slice(-1) === '|';
		if (hardEnd) {
                    rawPart = rawPart.slice(0, -1);
		}
		
		if ( rawPart === '' ) {
                    continue;
		}
		
		reStr = rawPart.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
		if (hardBeg) {
                    reStr = '(?:^|\\s)' + reStr;
		}
		if (hardEnd) {
                    reStr += '(?:\\s|$)';
		}
		
		reStrs.push(reStr);
		if (i < (n - 1) && rawParts[i + 1] === '||') {
                    i += 1;
                    continue;
		}
		
		reStr = reStrs.length === 1 ? reStrs[0] : reStrs.join('|');
		filters.push({
                    re: new RegExp(reStr, 'i'),
                    r: !not
		});
		reStrs = [];
		not = false;
            }
	};

	let filterOne = function (tr, clean) {
            let ff = filters;
            let fcount = ff.length;
            if (fcount === 0 && clean === true) {
		return;
            }
	    
            // do not filter out doc boundaries, they help separate
            // important section of log.
            let cl = tr.classList;
            if (cl.contains('doc')) {
		return;
            }
	    
            if (fcount === 0) {
		cl.remove('f');
		return;
            }
	    
            let cc = tr.cells;
            let ccount = cc.length;
            let hit, j, f;
	    
            // each filter expression must hit (implicit and-op)
            // if...
            //   positive filter expression = there must one hit on any field
            //   negative filter expression = there must be no hit on all fields
            for (let i=0; i<fcount; ++i) {
		f = ff[i];
		hit = !f.r;
		
		for (j=0; j<ccount; ++j) {
                    if (f.re.test(cc[j].textContent)) {
			hit = f.r;
			break;
                    }
		}
		
		if (!hit) {
                    cl.add('f');
                    return;
		}
            }
	    
            cl.remove('f');
	};

	let filterAll = function () {
            // Special case: no filter
            if (filters.length === 0) {
		uDom('#content tr').removeClass('f');
		return;
            }
	    
            let tbody = document.querySelector('#content tbody');
            let rows = tbody.rows;
            for (let i=rows.length-1; i>=0; --i) {
		filterOne(rows[i]);
            }
	};

	let onFilterChangedAsync = (function () {
            let timer = null;
            let commit = function () {
		timer = null;
		parseInput();
		filterAll();
            };
	    
            return function () {
		if (timer !== null) {
                    clearTimeout(timer);
		}
		timer = vAPI.setTimeout(commit, 750);
            };
	})();

	let onFilterButton = function () {
            let cl = document.body.classList;
            cl.toggle('f', cl.contains('f') === false);
	};

	uDom('#filterButton').on('click', onFilterButton);
	uDom('#filterInput').on('input', onFilterChangedAsync);

	return {
            filterOne: filterOne,
            filterAll: filterAll,
	};
    })();

    let toJunkyard = function (trs) {
	trs.remove();
	for (let i=trs.length-1; i>=0; --i) {
            trJunkyard.push(trs.nodeAt(i));
	}
    };

    let clearBuffer = function () {
	let tbody = document.querySelector('#content tbody');
	let tr;
	
	while (tbody.firstChild !== null) {
            tr = tbody.lastElementChild;
            trJunkyard.push(tbody.removeChild(tr));
	}
	
	uDom('#clear').addClass('disabled');
	uDom('#clean').addClass('disabled');
    };

    let cleanBuffer = function () {
	let rows = uDom('#content tr.tab:not(.canMtx)').remove();
	for (let i=rows.length-1; i>=0; --i) {
            trJunkyard.push(rows.nodeAt(i));
	}
	uDom('#clean').addClass('disabled');
    };

    let toggleCompactView = function () {
	document.body.classList.toggle('compactView');
	uDom('#content table .vExpanded').removeClass('vExpanded');
    };

    let toggleCompactRow = function (ev) {
	ev.target.parentElement.classList.toggle('vExpanded');
    };

    let popupManager = (function () {
	let realTabId = null;
	let localTabId = null;
	let container = null;
	let popup = null;
	let popupObserver = null;
	let style = null;
	let styleTemplate = [
            'tr:not(.tab_{{tabId}}) {',
            'cursor: not-allowed;',
            'opacity: 0.2;',
            '}'
	].join('\n');

	let resizePopup = function () {
            if (popup === null) {
		return;
            }
	    
            let popupBody = popup.contentWindow.document.body;
            if (popupBody.clientWidth !== 0
		&& container.clientWidth !== popupBody.clientWidth) {
		container.style.setProperty('width', popupBody.clientWidth + 'px');
            }
	    
            popup.style.removeProperty('height');
            if (popupBody.clientHeight !== 0
		&& popup.clientHeight !== popupBody.clientHeight) {
		popup.style.setProperty('height', popupBody.clientHeight + 'px');
            }
	    
            let ph = document.documentElement.clientHeight;
            let crect = container.getBoundingClientRect();
            if (crect.height > ph) {
		popup.style.setProperty('height', 'calc(' + ph + 'px - 1.8em)');
            }
	    
            // Adjust width for presence/absence of vertical scroll bar which may
            // have appeared as a result of last operation.
            let cw = container.clientWidth;
            let dw = popup.contentWindow.document.documentElement.clientWidth;
            if (cw !== dw) {
		container.style.setProperty('width', (2 * cw - dw) + 'px');
            }
	};

	let toggleSize = function () {
            container.classList.toggle('hide');
	};

	let onResizeRequested = function () {
            let popupBody = popup.contentWindow.document.body;
            if (popupBody.hasAttribute('data-resize-popup') === false) {
		return;
            }
	    
            popupBody.removeAttribute('data-resize-popup');
            resizePopup();
	};

	let onLoad = function () {
            resizePopup();
            let popupBody = popup.contentDocument.body;
            popupBody.removeAttribute('data-resize-popup');
            popupObserver.observe(popupBody, {
		attributes: true,
		attributesFilter: [ 'data-resize-popup' ]
            });
	};

	let toggleOn = function (td) {
            let tr = td.parentNode;
            let matches = tr.className.match(/(?:^| )tab_([^ ]+)/);
            if (matches === null) {
		return;
            }
	    
            realTabId = localTabId = matches[1];
            if (localTabId === 'bts') {
		realTabId = noTabId;
            }

            container = document.getElementById('popupContainer');

            container
		.querySelector('div > span:nth-of-type(1)')
		.addEventListener('click', toggleSize);
            container
		.querySelector('div > span:nth-of-type(2)')
		.addEventListener('click', toggleOff);

            popup = document.createElement('iframe');
            popup.addEventListener('load', onLoad);
            popup.setAttribute('src', 'popup.html?tabId=' + realTabId);
            popupObserver = new MutationObserver(onResizeRequested);
            container.appendChild(popup);

            style = document.getElementById('popupFilterer');
            style.textContent = styleTemplate.replace('{{tabId}}', localTabId);

            document.body.classList.add('popupOn');
	};

	let toggleOff = function () {
            document.body.classList.remove('popupOn');

            container
		.querySelector('div > span:nth-of-type(1)')
		.removeEventListener('click', toggleSize);
            container
		.querySelector('div > span:nth-of-type(2)')
		.removeEventListener('click', toggleOff);
            container.classList.remove('hide');

            popup.removeEventListener('load', onLoad);
            popupObserver.disconnect();
            popupObserver = null;
            popup.setAttribute('src', '');
            container.removeChild(popup);
            popup = null;

            style.textContent = '';
            style = null;

            container = null;
            realTabId = null;
	};

	let exports = {
            toggleOn: function (ev) {
		if (realTabId === null) {
                    toggleOn(ev.target);
		}
            },
            toggleOff: function () {
		if (realTabId !== null) {
                    toggleOff();
		}
            }
	};

	Object.defineProperty(exports, 'tabId', {
            get: function () {
		return realTabId || 0;
	    },
	});

	return exports;
    })();

    let grabView = function () {
	if (ownerId === undefined) {
            ownerId = Date.now();
	}
	readLogBufferAsync();
    };

    let releaseView = function () {
	if (ownerId === undefined) {
	    return;
	}
	
	vAPI.messaging.send('logger-ui.js', {
	    what: 'releaseView',
	    ownerId: ownerId
	});
	
	ownerId = undefined;
    };

    window.addEventListener('pagehide', releaseView);
    window.addEventListener('pageshow', grabView);
    // https://bugzilla.mozilla.org/show_bug.cgi?id=1398625
    window.addEventListener('beforeunload', releaseView);

    readLogBuffer();

    uDom('#pageSelector').on('change', pageSelectorChanged);
    uDom('#refresh').on('click', refreshTab);
    uDom('#compactViewToggler').on('click', toggleCompactView);
    uDom('#clean').on('click', cleanBuffer);
    uDom('#clear').on('click', clearBuffer);
    uDom('#maxEntries').on('change', onMaxEntriesChanged);
    uDom('#content table').on('click', 'tr > td:nth-of-type(1)',
			      toggleCompactRow);
    uDom('#content table').on('click', 'tr.canMtx > td:nth-of-type(2)',
			      popupManager.toggleOn);
})();
