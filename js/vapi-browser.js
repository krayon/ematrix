/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 The uMatrix/uBlock Origin authors
    Copyright (C) 2019-2020 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

/******************************************************************************/

(function () {
    vAPI.browser = {};
    
    vAPI.browser.getTabBrowser = function (win) {
	return win && win.gBrowser || null;
    };

    vAPI.browser.getOwnerWindow = function (target) {
	if (target.ownerDocument) {
            return target.ownerDocument.defaultView;
	}
	
	return null;
    };

    vAPI.browser.settings = {
	// For now, only booleans.
	originalValues: {},

	rememberOriginalValue: function (path, setting) {
            let key = path + '.' + setting;
            if (this.originalValues.hasOwnProperty(key)) {
		return;
            }
	    
            let hasUserValue;
            let branch = Services.prefs.getBranch(path + '.');
	    
            try {
		hasUserValue = branch.prefHasUserValue(setting);
            } catch (ex) {
		// Ignore
            }
	    
            if (hasUserValue !== undefined) {
		this.originalValues[key] = hasUserValue
		    ? this.getValue(path, setting)
		    : undefined;
            }
	},
	clear: function (path, setting) {
            let key = path + '.' + setting;

            // Value was not overriden -- nothing to restore
            if (this.originalValues.hasOwnProperty(key) === false) {
		return;
            }

            let value = this.originalValues[key];
	    
            // https://github.com/gorhill/uBlock/issues/292#issuecomment-109621979
            // Forget the value immediately, it may change outside of
            // uBlock control.
            delete this.originalValues[key];

            // Original value was a default one
            if (value === undefined) {
		try {
                    Services.prefs.getBranch(path + '.').clearUserPref(setting);
		} catch (ex) {
		    // Ignore
		}
		return;
            }

            // Reset to original value
            this.setValue(path, setting, value);
	},
	getValue: function (path, setting) {
            let branch = Services.prefs.getBranch(path + '.');

	    try {
		switch (branch.getPrefType(setting)) {
		case branch.PREF_INT:
		    return branch.getIntPref(setting);
		case branch.PREF_BOOL:
		    return branch.getBoolPref(setting);
		default:
		    // not supported
		    return;
		}
	    } catch (e) {
		// Ignore
	    }
	},
	setValue: function (path, setting, value) {
	    let branch = Services.prefs.getBranch(path + '.');
	    
	    try {
		switch (typeof value) {
		case 'number':
		    return branch.setIntPref(setting, value);
		case 'boolean':
		    return branch.setBoolPref(setting, value);
		default:
		    // not supported
		    return;
		}
	    } catch (e) {
		// Ignore
	    }
	},
	setSetting: function (setting, value) {
            switch (setting) {
            case 'prefetching':
		this.rememberOriginalValue('network', 'prefetch-next');
		// https://bugzilla.mozilla.org/show_bug.cgi?id=814169
		// Sigh.
		// eMatrix: doesn't seem the case for Pale
		// Moon/Basilisk, but let's keep this anyway
		this.rememberOriginalValue('network.http', 'speculative-parallel-limit');
		
		// https://github.com/gorhill/uBlock/issues/292
		// "true" means "do not disable", i.e. leave entry alone
		if (value) {
                    this.clear('network', 'prefetch-next');
                    this.clear('network.http', 'speculative-parallel-limit');
		} else {
                    this.setValue('network', 'prefetch-next', false);
                    this.setValue('network.http',
				  'speculative-parallel-limit', 0);
		}
		break;
            case 'hyperlinkAuditing':
		this.rememberOriginalValue('browser', 'send_pings');
		this.rememberOriginalValue('beacon', 'enabled');
		
		// https://github.com/gorhill/uBlock/issues/292
		// "true" means "do not disable", i.e. leave entry alone
		if (value) {
                    this.clear('browser', 'send_pings');
                    this.clear('beacon', 'enabled');
		} else {
                    this.setValue('browser', 'send_pings', false);
                    this.setValue('beacon', 'enabled', false);
		}
		break;
            case 'webrtcIPAddress':
		let prefName;
		let prefVal;
		
		// https://github.com/gorhill/uBlock/issues/894
		// Do not disable completely WebRTC if it can be avoided. FF42+
		// has a `media.peerconnection.ice.default_address_only` pref which
		// purpose is to prevent local IP address leakage.
		if (this.getValue('media.peerconnection',
				  'ice.default_address_only') !== undefined) {
                    prefName = 'ice.default_address_only';
                    prefVal = true;
		} else {
                    prefName = 'enabled';
                    prefVal = false;
		}

		this.rememberOriginalValue('media.peerconnection', prefName);
		if (value) {
                    this.clear('media.peerconnection', prefName);
		} else {
                    this.setValue('media.peerconnection', prefName, prefVal);
		}
		break;
            default:
		break;
            }
	},
	set: function (details) {
            for (let setting in details) {
		if (details.hasOwnProperty(setting) === false) {
                    continue;
		}
		this.setSetting(setting, !!details[setting]);
            }
	},
	restoreAll: function () {
            let pos;
            for (let key in this.originalValues) {
		if (this.originalValues.hasOwnProperty(key) === false) {
                    continue;
		}
		
		pos = key.lastIndexOf('.');
		this.clear(key.slice(0, pos), key.slice(pos + 1));
            }
	},
    };

    vAPI.addCleanUpTask(vAPI.browser.settings
			.restoreAll.bind(vAPI.browser.settings));

    vAPI.browser.data = {};

    vAPI.browser.data.clearCache = function (callback) {
	// PURGE_DISK_DATA_ONLY:1
	// PURGE_DISK_ALL:2
	// PURGE_EVERYTHING:3
	// However I verified that no argument does clear the cache data.
	// There is no cache2 for older versions of Firefox.
	if (Services.cache2) {
            Services.cache2.clear();
	} else if (Services.cache) {
            Services.cache.evictEntries(Services.cache.STORE_ON_DISK);
	}
	
	if (typeof callback === 'function') {
            callback();
	}
    };

    vAPI.browser.data.clearOrigin = function(/* domain */) {
	// TODO
	// eMatrix: is this actually needed? I don't really know what
	// it's supposed to do anyway.
    };
})();
