# ηMatrix

Definitely for advanced users.

***

## Install

The extension can be installed either by obtaining it from the
official add-ons websites, or by manually building the XPI.

### Official add-on repository

+ Pale Moon: http://addons.palemoon.org/addon/ematrix/
+ Basilisk: http://addons.basilisk-browser.org/addon/ematrix/

### Building the XPI

Clone the repository with git, then run `make all`.  
The XPI will be called `eMatrix.xpi`.

## Warning

### Regarding broken sites

ηMatrix will definitely break the majority of websites you visit.  
This add-on is meant for people that understand this and have enough
knoweledge to understand what it needs to be done to "unbreak" those
site.

## Other

For now, check the [μMatrix](https://github.com/gorhill/umatrix/)
home.

## License

[GPLv3](LICENSE.txt)
