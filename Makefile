# *****************************************************************************

#    ηMatrix - a browser extension to black/white list requests.
#    Copyright (C) 2019-2020 Alessio Vanni

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see {http://www.gnu.org/licenses/}.

#    Home: https://gitlab.com/vannilla/ematrix
#    uMatrix Home: https://github.com/gorhill/uMatrix

# To build the XPI we explicitly list every file for these reasons:
# 1) Some editors (like mine) automatically generate a backup file,
#    and always removing it before building the archive is bothersome
#    (and I don't trust my scripting skills to make it automatic)
# 2) It gives some flexibility in that files can be created or deleted
#    freely as long as they are not part of any of these lists

ROOTMETA := bootstrap.js install.rdf chrome.manifest icon.png	\
	    LICENSE.txt options.xul

ROOTHTML := about.html asset-viewer.html background.html	\
	    cloud-ui.html dashboard.html hosts-files.html	\
	    logger-ui.html main-blocked.html popup.html		\
	    raw-settings.html settings.html user-rules.html

ROOTOTHER := frameScript.js

ROOT := $(ROOTMETA) $(ROOTHTML) $(ROOTOTHER)

ASSETS := assets/assets.json

CSSSHEET := css/cloud-ui.css css/common.css css/dashboard-common.css	\
	    css/dashboard.css css/hosts-files.css			\
	    css/legacy-toolbar-button.css css/logger-ui.css		\
	    css/popup.css css/raw-settings.css css/user-rules.css

CSSFONTS := css/fonts/fontawesome-webfont.ttf				\
	    css/fonts/Roboto_Condensed/LICENSE.txt			\
	    css/fonts/Roboto_Condensed/RobotoCondensed-Bold.ttf		\
	    css/fonts/Roboto_Condensed/RobotoCondensed-Light.ttf	\
	    css/fonts/Roboto_Condensed/RobotoCondensed-Regular.ttf

CSS := $(CSSSHEET) $(CSSFONTS)

IMGICON := img/browsericons/icon19-0.png	\
	   img/browsericons/icon19-1.png	\
	   img/browsericons/icon19-2.png	\
	   img/browsericons/icon19-3.png	\
	   img/browsericons/icon19-4.png	\
	   img/browsericons/icon19-5.png	\
	   img/browsericons/icon19-6.png	\
	   img/browsericons/icon19-7.png	\
	   img/browsericons/icon19-8.png	\
	   img/browsericons/icon19-9.png	\
	   img/browsericons/icon19-10.png	\
	   img/browsericons/icon19-11.png	\
	   img/browsericons/icon19-12.png	\
	   img/browsericons/icon19-13.png	\
	   img/browsericons/icon19-14.png	\
	   img/browsericons/icon19-15.png	\
	   img/browsericons/icon19-16.png	\
	   img/browsericons/icon19-17.png	\
	   img/browsericons/icon19-18.png	\
	   img/browsericons/icon19-19.png	\
	   img/browsericons/icon38-off.png	\
	   img/browsericons/icon19-off.png

IMGOTHER := img/cloud.png img/icon_16.png img/icon_64.png		\
	    img/matrix-group-hide.png img/matrix-group-hline.png	\
	    img/matrix-group-show.png img/permanent-black-small.png	\
	    img/permanent-black-small-cb.png				\
	    img/permanent-white-small.png				\
	    img/permanent-black-small-cb.png

IMG := $(IMGICON) $(IMGOTHER)

LIB := lib/PublicSuffixList.jsm lib/Punycode.jsm lib/FrameModule.jsm	\
       lib/CallbackWrapper.jsm lib/HttpRequestHeaders.jsm		\
       lib/PendingRequests.jsm lib/CookieCache.jsm lib/UriTools.jsm	\
       lib/LiquidDict.jsm lib/HostMap.jsm lib/Tools.jsm			\
       lib/RowSnapshot.jsm

# The locale folder is split into parts because it's a long list
# and it's somewhat easier to read this way
LOC1 := locale/en/messages.properties locale/bg/messages.properties	\
	locale/cs/messages.properties locale/de/messages.properties	\
	locale/el/messages.properties

LOC2 := locale/es/messages.properties locale/fr/messages.properties	\
	locale/hu/messages.properties locale/id/messages.properties	\
	locale/it/messages.properties

LOC3 := locale/ja/messages.properties locale/ko/messages.properties	\
	locale/lv/messages.properties locale/ml/messages.properties	\
	locale/nb/messages.properties locale/nl/messages.properties	\
	locale/pl/messages.properties

LOC4 := locale/pt-BR/messages.properties				\
	locale/pt-PT/messages.properties				\
	locale/ro/messages.properties locale/ru/messages.properties	\
	locale/sl/messages.properties

LOC5 := locale/sr/messages.properties locale/sv/messages.properties	\
	locale/te/messages.properties locale/tr/messages.properties	\
	locale/uk/messages.properties locale/vi/messages.properties	\
	locale/zh-CN/messages.properties				\
	locale/zh-TW/messages.properties

LOCALE := $(LOC1) $(LOC2) $(LOC3) $(LOC4) $(LOC5)

# The js folder is split into parts because it's a long list
# and it's somewhat easier to read this way
JS1 := js/about.js js/assets.js js/asset-viewer.js js/background.js	\
       js/browsercache.js js/cloud-ui.js js/contentscript.js		\
       js/contentscript-start.js js/cookies.js js/dashboard-common.js	\
       js/dashboard.js js/hosts-files.js

JS2 := js/httpsb.js js/i18n.js js/logger.js js/logger-ui.js		\
       js/main-blocked.js js/matrix.js js/messaging.js			\
       js/pagestats.js js/popup.js js/profiler.js js/raw-settings.js	\
       js/settings.js js/start.js js/storage.js

JS3 := js/tab.js js/traffic.js js/udom.js js/user-rules.js	\
       js/usersettings.js js/xal.js

JS4 := js/vapi-client.js js/vapi-common.js js/vapi-background.js	\
       js/vapi-tabs.js js/vapi-window.js js/vapi-core.js		\
       js/vapi-browser.js js/vapi-storage.js js/vapi-messaging.js	\
       js/vapi-net.js js/vapi-cookies.js js/vapi-cloud.js

JS := $(JS1) $(JS2) $(JS3) $(JS4)

all: eMatrix.xpi

eMatrix.xpi: $(ROOT) $(ASSETS) $(CSS) $(IMG) $(LIB) $(LOCALE) $(JS)
	@zip -r $@ $^

.PHONY: clean

clean:
	@rm -f eMatrix.xpi
