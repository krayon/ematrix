/*******************************************************************************

    ηMatrix - a browser extension to black/white list requests.
    Copyright (C) 2014-2019 The uMatrix/uBlock Origin authors
    Copyright (C) 2019-2020 Alessio Vanni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see {http://www.gnu.org/licenses/}.

    Home: https://gitlab.com/vannilla/ematrix
    uMatrix Home: https://github.com/gorhill/uMatrix
*/

'use strict';

var EXPORTED_SYMBOLS = ['HTTPRequestHeaders'];

var junkyard = [];

var HTTPRequestHeaders = function (channel) {
    this.init(channel);
};

HTTPRequestHeaders.factory = function (channel) {
    let entry = junkyard.pop();
    if (entry) {
	return entry.init(channel);
    }

    return new HTTPRequestHeaders(channel);
}

HTTPRequestHeaders.prototype.init = function (channel) {
    this.channel = channel;
    this.headers = new Array();
    this.originalHeaderNames = new Array();
    
    channel.visitRequestHeaders({
	visitHeader: function (name, value) {
	    this.headers.push({name: name, value: value});
	    this.originalHeaderNames.push(name);
	}.bind(this)
    });
    
    return this;
};

HTTPRequestHeaders.prototype.dispose = function () {
    this.channel = null;
    this.headers = null;
    this.originalHeaderNames = null;
    junkyard.push(this);
};

HTTPRequestHeaders.prototype.update = function () {
    let newHeaderNames = new Set();
    for (let header of this.headers) {
        this.setHeader(header.name, header.value, true);
        newHeaderNames.add(header.name);
    }
    
    //Clear any headers that were removed
    for (let name of this.originalHeaderNames) {
        if (!newHeaderNames.has(name)) {
	    this.channel.setRequestHeader(name, '', false);
        }
    }
};

HTTPRequestHeaders.prototype.getHeader = function (name) {
    try {
        return this.channel.getRequestHeader(name);
    } catch (e) {
	// Ignore
    }
    
    return '';
};

HTTPRequestHeaders.prototype.setHeader = function (name, newValue, create) {
    let oldValue = this.getHeader(name);
    if (newValue === oldValue) {
        return false;
    }
    
    if (oldValue === '' && create !== true) {
        return false;
    }
    
    this.channel.setRequestHeader(name, newValue, false);
    return true;
};
